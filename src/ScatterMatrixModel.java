import processing.core.PVector;

public class ScatterMatrixModel {

    // Value on the x-axis for a single scatterplot in the scattermatrix.
    private String xProperty;


    // Value on the y-axis for a single scatterplot in the scattermatrix.
    private String yProperty;

    // Starting postion of a rectangle in the scattermatrix.
    private PVector rectanglePositon;

    public ScatterMatrixModel(String xProperty, String yProperty, PVector rectanglePositon) {
        this.xProperty = xProperty;
        this.yProperty = yProperty;
        this.rectanglePositon = rectanglePositon;
    }

    public String getxProperty() {
        return xProperty;
    }

    public void setxProperty(String xProperty) {
        this.xProperty = xProperty;
    }

    public String getyProperty() {
        return yProperty;
    }

    public void setyProperty(String yProperty) {
        this.yProperty = yProperty;
    }

    public PVector getRectanglePositon() {
        return rectanglePositon;
    }

    public void setRectanglePositon(PVector rectanglePositon) {
        this.rectanglePositon = rectanglePositon;
    }
}
