import processing.core.PApplet;

public class Scatterplot extends PApplet {

    private ScatterplotModel scatterplotModel;

    // Constants for min and max EIG1 and EIG2 values.
    private int MIN_SCATTERPLOT_X_VALUE;
    private int MIN_SCATTERPLOT_Y_VALUE;

    private int MAX_SCATTERPLOT_X_VALUE;
    private int MAX_SCATTERPLOT_Y_VALUE;

    public void setup(){
        size(600, 500);
        scatterplotModel = loadData();
        noLoop();
    }

    public void draw(){
        drawLegend();
        drawAxes();
        drawPoints();
        drawTitle();
    }

    private void drawTitle(){
        textAlign(CENTER, TOP);
        textSize(16);
        text("Scatterplot - Door Erik Euser en Johan Boers.", 250, 50);
    }

    private void drawLegend(){
        // Legend wrapper
        fill(256, 256, 256);
        rect(450, 200, 100, 100);

        // Red
        fill(255, 0 ,0);
        rect(460, 210, 25, 15);

        // Blue
        fill(0, 152 , 255);
        rect(460, 230, 25, 15);

        // Green
        fill(0, 204 ,0);
        rect(460, 250, 25, 15);

        // Black
        fill(0, 0 ,0);
        rect(460, 270, 25, 15);

        // Legend discription
        textSize(10);
        text("CAT = 1", 495, 221);
        text("CAT = 2", 495, 241);
        text("CAT = 3", 495, 261);
        text("CAT = 4", 495, 281);
    }

    private void drawAxes(){
        textAlign(CENTER, TOP);

        // X-axis
        line(100, 400, 400, 400);
        fill(0,0,0);

        text(MIN_SCATTERPLOT_X_VALUE, 105, 410);
        text("EIG1", 250, 410);
        text(MAX_SCATTERPLOT_X_VALUE, 395, 410);
        fill(0,0,0);

        // Y-axis
        line(100, 100, 100, 400);
        fill(0,0,0);

        text(MIN_SCATTERPLOT_Y_VALUE, 80, 380);
        text("EIG2", 80, 250);
        text(MAX_SCATTERPLOT_Y_VALUE, 80, 100);
        fill(0,0,0);
    }

    private void drawPoints(){
        for (int i = 0; i < scatterplotModel.getEig1().length; i++) {
            final float mappedX = map(scatterplotModel.getEig1()[i], MIN_SCATTERPLOT_X_VALUE, MAX_SCATTERPLOT_X_VALUE, 100, 400);
            final float mappedY = map(scatterplotModel.getEig2()[i], MIN_SCATTERPLOT_Y_VALUE, MAX_SCATTERPLOT_Y_VALUE, 100, 400);

            strokeWeight(3);
            final int[] rgb = determinePointColor(scatterplotModel.getCat()[i]);
            stroke(rgb[0], rgb[1], rgb[2]);
            point(mappedX, mappedY);
        }
    }


    private ScatterplotModel loadData(){
        final String[] scatterplotData = loadStrings("resources/scatterplot.txt");

        final int[] eig1Array   = new int[scatterplotData.length];
        final float[] eig2Array   = new float[scatterplotData.length];
        final int[] catArray  = new int[scatterplotData.length];

        for (int i = 0; i < scatterplotData.length; i++) {

            final String[] row = scatterplotData[i].split("\t");

            eig1Array[i] = Integer.parseInt(row[1]);
            eig2Array[i] = Float.parseFloat(row[2]);
            catArray[i]  = Integer.parseInt(row[0]);
        }

        // Determine min and max values of the X and Y-axis.
        MIN_SCATTERPLOT_X_VALUE = min(eig1Array);
        MAX_SCATTERPLOT_X_VALUE = max(eig1Array);
        MIN_SCATTERPLOT_Y_VALUE = (int) min(eig2Array);
        MAX_SCATTERPLOT_Y_VALUE = (int) max(eig2Array);

        return new ScatterplotModel(catArray,eig1Array, eig2Array);
    }

    /**
     * 1 = Red.
     * 2 = Blue
     * 3 = Green.
     * 4 = Black.
     * */
    private int[] determinePointColor(int cat){
        final int[] rgb = new int[3];
        switch (cat){
            case 1 :
                rgb[0] = 255;
                rgb[1] = 0;
                rgb[2] = 0;
                break;
            case 2 :
                rgb[0] = 0;
                rgb[1] = 152;
                rgb[2] = 255;
                break;
            case 3 :
                rgb[0] = 0;
                rgb[1] = 204;
                rgb[2] = 0;
                break;
            case 4 :
                rgb[0] = 0;
                rgb[1] = 0;
                rgb[2] = 0;
                break;
            default:
                rgb[0] = 0;
                rgb[1] = 0;
                rgb[2] = 0;
                break;
        }

        return rgb;
    }
}
