import processing.core.PApplet;

/**
 * @author Erik en Johan
 */
public class Main {
    public static void main(String[] args) {
        PApplet.main("ScatterMatrix");
        PApplet.main("Scatterplot");
    }
}
