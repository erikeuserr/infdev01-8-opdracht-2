import processing.core.PApplet;
import processing.core.PVector;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * Deze scattermatrix toont een correlatie tussen leeftijd, analyse cijfers, dev cijfers, en project cijfers.
 * @author Erik en Johan
 */
public class ScatterMatrix extends PApplet {

    private ScatterMatrixData scattermatrixData;
    private ArrayList<ScatterMatrixModel> scattermatrixModels;

    private static final int RECTANGLE_WIDTH = 150;
    private static final int RECTANGLE_HEIGHT = 100;

    public void setup(){
        size(900,600);
        scattermatrixData = loadData();
        scattermatrixModels = new ArrayList<ScatterMatrixModel>();

        noLoop();
    }

    public void draw(){
        drawAxes();
        drawPoints();
        drawTitle();
    }

    private void drawTitle(){
        textAlign(CENTER, TOP);
        textSize(16);
        text("Scattermatrix of Age, Grades for Analyse, Grades for Development and Grades for Project.", 450, 50);

        textSize(14);
        text("Door Erik Euser en Johan Boers.", 450, 75);
    }

    private void drawPoints(){
        textAlign(CENTER, TOP);
        strokeWeight(2);
        stroke(0, 0, 255);

        int scatterModelCount = 0;
        // Draw points for 16 scattermatrixmodels in the list.
        for(ScatterMatrixModel scatterMatrixModel : scattermatrixModels){
            final PVector position    = scatterMatrixModel.getRectanglePositon();

            final float[] xAxisValues = determineProperty(scatterMatrixModel.getxProperty());
            final float[] yAxisValues = determineProperty(scatterMatrixModel.getyProperty());

            // Determine which properties to compare.
            if(!scatterMatrixModel.getxProperty().equals(scatterMatrixModel.getyProperty())){

                for (int i = 0; i < xAxisValues.length; i++) {
                    // Mapping range is smaller then to size of the rectangles (140 by 90 px instead of 150 by 100), this is to fit them inside of the rectangles.
                    final float mappedX = map(xAxisValues[i], (int) min(xAxisValues), (int) max(xAxisValues), position.x + 10, position.x + (RECTANGLE_WIDTH - 10));
                    final float mappedY = map(yAxisValues[i], (int) min(yAxisValues), (int) max(yAxisValues), position.y + 10, position.y + (RECTANGLE_HEIGHT - 10));

                    point(mappedX, mappedY);
                }
            } else {

                // Set text in the center of the rectangle.
                text(scatterMatrixModel.getxProperty(), position.x + 75, position.y + 50);

                // If Age and Age draw description for age at a specific position.
                if(scatterModelCount == 0){
                    ScatterMatrixModel model4  = scattermatrixModels.get(3);
                    ScatterMatrixModel model13 = scattermatrixModels.get(12);

                    // Draw description to the right of rectangle 4.
                    text((int) min(yAxisValues), model4.getRectanglePositon().x + RECTANGLE_WIDTH + 15, model4.getRectanglePositon().y + RECTANGLE_HEIGHT - 20);
                    text((int) max(yAxisValues), model4.getRectanglePositon().x + RECTANGLE_WIDTH + 15, model4.getRectanglePositon().y + 7);

                    // Draw a description below rectangle 13.
                    text((int) min(xAxisValues), model13.getRectanglePositon().x + 10, model13.getRectanglePositon().y + RECTANGLE_HEIGHT +  7);
                    text((int) max(xAxisValues), model13.getRectanglePositon().x + RECTANGLE_WIDTH - 10, model13.getRectanglePositon().y + RECTANGLE_HEIGHT +  7);
                }

                else if(scatterModelCount == 5){
                    final ScatterMatrixModel model2  = scattermatrixModels.get(1);
                    final ScatterMatrixModel model5  = scattermatrixModels.get(4);

                    // Draw description above rectangle 2.
                    text((int) min(xAxisValues), model2.getRectanglePositon().x + 10, model2.getRectanglePositon().y - 20);
                    text((int) max(xAxisValues), model2.getRectanglePositon().x + RECTANGLE_WIDTH - 10, model2.getRectanglePositon().y - 20);

                    // Draw a description to the left of rectangle 5.
                    text((int) min(yAxisValues), model5.getRectanglePositon().x - 15, model5.getRectanglePositon().y + RECTANGLE_HEIGHT - 20);
                    text((int) max(yAxisValues), model5.getRectanglePositon().x - 15, model5.getRectanglePositon().y +  7);
                }
                else if(scatterModelCount == 10){
                    final ScatterMatrixModel model12  = scattermatrixModels.get(11);
                    final ScatterMatrixModel model15  = scattermatrixModels.get(14);

                    // Draw description to the right of rectangle 12.
                    text((int) min(yAxisValues), model12.getRectanglePositon().x + RECTANGLE_WIDTH + 15, model12.getRectanglePositon().y + RECTANGLE_HEIGHT - 20);
                    text((int) max(yAxisValues), model12.getRectanglePositon().x + RECTANGLE_WIDTH + 15, model12.getRectanglePositon().y + 7);

                    // Draw a description
                    text((int) min(xAxisValues), model15.getRectanglePositon().x + 10, model15.getRectanglePositon().y + RECTANGLE_HEIGHT +  7);
                    text((int) max(xAxisValues), model15.getRectanglePositon().x + RECTANGLE_WIDTH - 10, model15.getRectanglePositon().y + RECTANGLE_HEIGHT +  7);
                }
                else if(scatterModelCount == 15){
                    final ScatterMatrixModel model4  = scattermatrixModels.get(3);
                    final ScatterMatrixModel model13  = scattermatrixModels.get(12);

                    // Draw description to the right of rectangle 12.
                    text((int) min(xAxisValues), model4.getRectanglePositon().x + 10, model4.getRectanglePositon().y - 20);
                    text((int) max(xAxisValues), model4.getRectanglePositon().x + RECTANGLE_WIDTH - 10, model4.getRectanglePositon().y - 20);

                    // Draw a description
                    text((int) min(yAxisValues), model13.getRectanglePositon().x - 15, model13.getRectanglePositon().y + RECTANGLE_HEIGHT - 20);
                    text((int) max(yAxisValues), model13.getRectanglePositon().x - 15, model13.getRectanglePositon().y +  7);
                }
            }

            scatterModelCount++;
        }
    }

    private float[] determineProperty(String property){
        if (property.equals("Age")) {
            return scattermatrixData.getAgeArray();
        } else if (property.equals("AnalyseGrades")) {
            return scattermatrixData.getAnalyseGrades();
        } else if (property.equals("DevGrades")) {
            return scattermatrixData.getDevGrades();
        } else if (property.equals("ProjGrades")){
            return scattermatrixData.getProjGrades();
        }

        // Should never happen.
        return scattermatrixData.getAgeArray();
    }

    private void drawAxes() {
        final List<String> propertyList = Arrays.asList("Age", "AnalyseGrades", "DevGrades", "ProjGrades");

        int x = 150;
        int y = 150;

        // Draw 16 Rectangles and create an object foreach 2 properties that can be compared.
        for (int i = 0; i < propertyList.size(); i++) {
            for (int j = 0; j < propertyList.size(); j++) {
                rect(x , y, RECTANGLE_WIDTH, RECTANGLE_HEIGHT);
                scattermatrixModels.add(new ScatterMatrixModel(propertyList.get(j), propertyList.get(i), new PVector((float) x, (float) y)));
                x += 150;
            }

            x = 150;
            y += 100;
        }
        fill(0,0,0);
    }

    private ScatterMatrixData loadData(){
        final String[] scatterMatrixData = loadStrings("resources/studentcijfers.txt");

        final float[] ageArray      = new float[scatterMatrixData.length];
        final float[] analyseGrades = new float[scatterMatrixData.length];
        final float[] devGrades     = new float[scatterMatrixData.length];
        final float[] projGrades    = new float[scatterMatrixData.length];

        for (int i = 0; i < scatterMatrixData.length; i++) {
            final String[] splittedData = scatterMatrixData[i].split("\t");
            ageArray[i]      = Float.parseFloat(splittedData[1]);
            analyseGrades[i] = Float.parseFloat(splittedData[2]);
            devGrades[i]     = Float.parseFloat(splittedData[3]);
            projGrades[i]    = Float.parseFloat(splittedData[4]);
        }

        return new ScatterMatrixData(ageArray, analyseGrades, devGrades, projGrades);
    }
}
