
public class ScatterMatrixData {
    private float[] ageArray;

    private float[] analyseGrades;

    private float[] devGrades;

    private float[] projGrades;

    public ScatterMatrixData(float[] ageArray, float[] analyseGrades, float[] devGrades, float[] projGrades) {
        this.ageArray = ageArray;
        this.analyseGrades = analyseGrades;
        this.devGrades = devGrades;
        this.projGrades = projGrades;
    }

    public float[] getAgeArray() {
        return ageArray;
    }

    public void setAgeArray(float[] ageArray) {
        this.ageArray = ageArray;
    }

    public float[] getAnalyseGrades() {
        return analyseGrades;
    }

    public void setAnalyseGrades(float[] analyseGrades) {
        this.analyseGrades = analyseGrades;
    }

    public float[] getDevGrades() {
        return devGrades;
    }

    public void setDevGrades(float[] devGrades) {
        this.devGrades = devGrades;
    }

    public float[] getProjGrades() {
        return projGrades;
    }

    public void setProjGrades(float[] projGrades) {
        this.projGrades = projGrades;
    }
}
