public class ScatterplotModel {

    private int[] cat;
    private int[] eig1;
    private float[] eig2;

    public ScatterplotModel(int[] cat, int[] eig1, float[] eig2) {
        this.cat = cat;
        this.eig1 = eig1;
        this.eig2 = eig2;
    }

    public int[] getCat() {
        return cat;
    }

    public void setCat(int[] cat) {
        this.cat = cat;
    }

    public int[] getEig1() {
        return eig1;
    }

    public void setEig1(int[] eig1) {
        this.eig1 = eig1;
    }

    public float[] getEig2() {
        return eig2;
    }

    public void setEig2(float[] eig2) {
        this.eig2 = eig2;
    }
}
